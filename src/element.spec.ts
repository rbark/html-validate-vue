import { processElement } from "./element";
import { HtmlElement, TextNode } from "html-validate/build/dom";

describe("slot metadata", () => {
	it("should load slot metadata", () => {
		const context = {
			getMetaFor: jest.fn().mockReturnValue({ flow: true }),
		};
		const parent = new HtmlElement("parent");
		const node = new HtmlElement("foo", parent);
		node.setAttribute("v-slot:slot-name", null, null, null);
		processElement.call(context, node);
		expect(context.getMetaFor).toHaveBeenCalledWith("parent:slot-name");
		expect(node.meta).toEqual({ flow: true });
	});

	it("should handle when slot is missing parent", () => {
		expect.assertions(1);
		const context = {
			getMetaFor: jest.fn().mockReturnValue({ flow: true }),
		};
		const node = new HtmlElement("foo");
		node.setAttribute("v-slot:slot-name", null, null, null);
		expect(() => processElement.call(context, node)).not.toThrow();
	});

	it("should handle when slot is missing metadata", () => {
		expect.assertions(1);
		const context = {
			getMetaFor: (): null => null,
		};
		const parent = new HtmlElement("parent");
		const node = new HtmlElement("foo", parent);
		node.setAttribute("v-slot:slot-name", null, null, null);
		expect(() => processElement.call(context, node)).not.toThrow();
	});

	it("should not crash with html-validate < 2.7 ", () => {
		expect.assertions(1);
		const node = new HtmlElement("foo");
		expect(() => processElement.call(undefined, node)).not.toThrow();
	});
});

test("should add dynamic text when v-html is present", () => {
	const node = new HtmlElement("foo");
	node.setAttribute("v-html", "bar", null, null);
	processElement.call(undefined, node);
	expect(node.childNodes).toEqual([expect.any(TextNode)]);
});

test("should add dynamic text when node is <slot>", () => {
	const node = new HtmlElement("slot");
	processElement.call(undefined, node);
	expect(node.childNodes).toEqual([expect.any(TextNode)]);
});

test("should not add dynamic text if no dynamic content attribute is present", () => {
	const node = new HtmlElement("foo");
	processElement.call(undefined, node);
	expect(node.childNodes).toEqual([]);
});
