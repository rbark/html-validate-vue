import { AvailableSlots } from "./available-slots";
import { RequiredSlots } from "./required-slots";

const rules = {
	"vue/available-slots": AvailableSlots,
	"vue/required-slots": RequiredSlots,
};

export default rules;
