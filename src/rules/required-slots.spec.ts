import { HtmlValidate } from "html-validate";
import "html-validate/build/matchers";
import Plugin from "..";

jest.mock("html-validate-vue", () => Plugin, { virtual: true });

describe("vue/required-slots", () => {
	let htmlvalidate: HtmlValidate;

	beforeAll(() => {
		htmlvalidate = new HtmlValidate({
			plugins: ["html-validate-vue"],
			elements: [
				{
					"my-component": {
						requiredSlots: ["foo"],
					},
				},
			],
			rules: { "vue/required-slots": "error" },
		});
	});

	it("should not report error when all required slots are implemented (v-slot)", () => {
		const report = htmlvalidate.validateString(
			"<my-component><template v-slot:foo></template></my-component>"
		);
		expect(report).toBeValid();
	});

	it("should not report error when all required slots are implemented (shorthand)", () => {
		const report = htmlvalidate.validateString(
			"<my-component><template #foo></template></my-component>"
		);
		expect(report).toBeValid();
	});

	it("should not report error when all required slots are implemented (slot)", () => {
		const report = htmlvalidate.validateString(
			'<my-component><template slot="foo"></template></my-component>'
		);
		expect(report).toBeValid();
	});

	it("should report error when component is missing a required slot", () => {
		const report = htmlvalidate.validateString("<my-component></my-component>");
		expect(report).toBeInvalid();
		expect(report).toHaveError(
			"vue/required-slots",
			'<my-component> component requires slot "foo" to be implemented'
		);
	});

	it("should contain documentation", () => {
		expect(
			htmlvalidate.getRuleDocumentation("vue/required-slots")
		).toMatchSnapshot();
	});

	it("should contain contextual documentation", () => {
		const context = {
			element: "my-component",
			slot: "missing-slot",
			required: ["foo", "bar", "baz"],
		};
		expect(
			htmlvalidate.getRuleDocumentation("vue/required-slots", null, context)
		).toMatchSnapshot();
	});
});
