import { HtmlValidate } from "html-validate";
import "html-validate/build/matchers";
import Plugin from "..";

jest.mock("html-validate-vue", () => Plugin, { virtual: true });

describe("vue/available-slots", () => {
	let htmlvalidate: HtmlValidate;

	beforeAll(() => {
		htmlvalidate = new HtmlValidate({
			plugins: ["html-validate-vue"],
			elements: [
				{
					"my-component": {
						slots: ["foo", "with-dashes"],
					},
				},
			],
			rules: { "vue/available-slots": "error" },
		});
	});

	it("should not report error when component uses no slots", () => {
		const report = htmlvalidate.validateString("<my-component></my-component>");
		expect(report).toBeValid();
	});

	it("should not report error when component uses available slot", () => {
		const report = htmlvalidate.validateString(
			"<my-component><template v-slot:foo></template></my-component>"
		);
		expect(report).toBeValid();
	});

	it("should not report error when component uses dynamic slot", () => {
		const report = htmlvalidate.validateString(
			"<my-component><template v-slot:[foo]></template></my-component>"
		);
		expect(report).toBeValid();
	});

	it("should not report error for slot with dashes", () => {
		const report = htmlvalidate.validateString(
			"<my-component><template v-slot:with-dashes></template></my-component>"
		);
		expect(report).toBeValid();
	});

	it("should report error when element has invalid attribute value (v-slot)", () => {
		const report = htmlvalidate.validateString(
			"<my-component><template v-slot:bar></template></my-component>"
		);
		expect(report).toBeInvalid();
		expect(report).toHaveError(
			"vue/available-slots",
			'<my-component> component has no slot "bar"'
		);
	});

	it("should report error when element has invalid attribute value (shorthand)", () => {
		const report = htmlvalidate.validateString(
			"<my-component><template #bar></template></my-component>"
		);
		expect(report).toBeInvalid();
		expect(report).toHaveError(
			"vue/available-slots",
			'<my-component> component has no slot "bar"'
		);
	});

	it("should report error when element has invalid attribute value (slot)", () => {
		const report = htmlvalidate.validateString(
			'<my-component><template slot="bar"></template></my-component>'
		);
		expect(report).toBeInvalid();
		expect(report).toHaveError(
			"vue/available-slots",
			'<my-component> component has no slot "bar"'
		);
	});

	it("should contain documentation", () => {
		expect(
			htmlvalidate.getRuleDocumentation("vue/available-slots")
		).toMatchSnapshot();
	});

	it("should contain contextual documentation", () => {
		const context = {
			element: "my-component",
			slot: "missing-slot",
			available: ["foo", "bar", "baz"],
		};
		expect(
			htmlvalidate.getRuleDocumentation("vue/available-slots", null, context)
		).toMatchSnapshot();
	});
});
