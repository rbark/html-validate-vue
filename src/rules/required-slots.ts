import { HtmlElement, Rule } from "html-validate";
import { DOMReadyEvent } from "html-validate/build/event";
import { RuleDocumentation } from "html-validate/build/rule";
import { findUsedSlots } from "./utils/slots";

declare module "html-validate/build/meta" {
	interface MetaElement {
		requiredSlots?: string[];
	}
}

interface Context {
	element: string;
	slot: string;
	required: string[];
}

function difference<T>(a: Set<T>, b: Set<T>): Set<T> {
	const result = new Set(a);
	for (const elem of b) {
		result.delete(elem);
	}
	return result;
}

export class RequiredSlots extends Rule<Context> {
	public documentation(context: Context): RuleDocumentation {
		if (context) {
			return {
				description: [
					`The <${context.element}> component requires slot "${context.slot}" to be implemented. Add \`<template v-slot:${context.slot}>\``,
					"",
					"The following slots are required",
				]
					.concat(context.required.map(slot => `- ${slot}`))
					.join("\n"),
			};
		} else {
			return {
				description: "All required slots must be implemented.",
			};
		}
	}

	public setup(): void {
		this.on("dom:ready", (event: DOMReadyEvent) => {
			const doc = event.document;
			doc.visitDepthFirst((node: HtmlElement) => {
				/* ignore rule if element has no meta or meta does not specify available slots */
				const meta = node.meta;
				if (!meta || !meta.requiredSlots) return;

				this.validateSlots(node);
			});
		});
	}

	private validateSlots(node: HtmlElement): void {
		const meta = node.meta;
		const usedSlots = findUsedSlots(node);
		const required = new Set(meta.requiredSlots);
		const used = new Set(Object.keys(usedSlots));
		const diff = difference(required, used);

		for (const missing of diff) {
			const context: Context = {
				element: node.tagName,
				slot: missing,
				required: meta.requiredSlots,
			};
			this.report(
				node,
				`<${node.tagName}> component requires slot "${missing}" to be implemented`,
				null,
				context
			);
		}
	}
}
