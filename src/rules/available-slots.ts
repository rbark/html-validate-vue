import { HtmlElement, Rule } from "html-validate";
import { DOMReadyEvent } from "html-validate/build/event";
import { RuleDocumentation } from "html-validate/build/rule";
import { findUsedSlots } from "./utils/slots";

declare module "html-validate/build/meta" {
	interface MetaElement {
		slots?: string[];
	}
}

interface Context {
	element: string;
	slot: string;
	available: string[];
}

function difference<T>(a: Set<T>, b: Set<T>): Set<T> {
	const result = new Set(a);
	for (const elem of b) {
		result.delete(elem);
	}
	return result;
}

export class AvailableSlots extends Rule<Context> {
	public documentation(context: Context): RuleDocumentation {
		if (context) {
			return {
				description: [
					`The <${context.element}> component does not have a slot named "${context.slot}". Only known slots can be specified.`,
					"",
					"The following slots are available",
				]
					.concat(context.available.map(slot => `- ${slot}`))
					.join("\n"),
			};
		} else {
			return {
				description: "Only known slots can be specified.",
			};
		}
	}

	public setup(): void {
		this.on("dom:ready", (event: DOMReadyEvent) => {
			const doc = event.document;
			doc.visitDepthFirst((node: HtmlElement) => {
				/* ignore rule if element has no meta or meta does not specify available slots */
				const meta = node.meta;
				if (!meta || !meta.slots) return;

				this.validateSlots(node);
			});
		});
	}

	private validateSlots(node: HtmlElement): void {
		const meta = node.meta;
		const usedSlots = findUsedSlots(node);
		const available = new Set(meta.slots);
		const used = new Set(Object.keys(usedSlots));
		const diff = difference(used, available);

		for (const missing of diff) {
			const context: Context = {
				element: node.tagName,
				slot: missing,
				available: meta.slots,
			};
			this.report(
				node,
				`<${node.tagName}> component has no slot "${missing}"`,
				usedSlots[missing],
				context
			);
		}
	}
}
