import { ConfigData } from "html-validate";
import path from "path";

const config: ConfigData = {
	elements: [path.join(__dirname, "../../elements.json")],
	rules: {
		/* vue modifiers often use camelcase so allow by default */
		"attr-case": ["error", { style: "camelcase" }],

		/* enable rules from this plugin */
		"vue/available-slots": "error",
		"vue/required-slots": "error",
	},
};

export = config;
