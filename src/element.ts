import {
	DynamicValue,
	HtmlElement,
	ProcessElementContext,
} from "html-validate";
import { findSlotAttribute } from "./rules/utils/slots";

function isBound(node: HtmlElement): boolean {
	return node.hasAttribute("v-html");
}

function isSlot(node: HtmlElement): boolean {
	return node.is("slot");
}

function haveGetMetaFor(context: ProcessElementContext): boolean {
	return !!(context && context.getMetaFor);
}

function loadSlotMeta(
	context: ProcessElementContext,
	slot: string,
	node: HtmlElement
): void {
	const parent = node.parent;
	if (!parent) {
		return;
	}

	/* Generate a new virtual tagname to load meta element from */
	const key = `${parent.tagName}:${slot}`;
	const meta = context.getMetaFor(key);
	if (!meta) {
		return;
	}

	node.loadMeta(meta);
	node.setAnnotation(`slot "${slot}" (<${parent.tagName}>)`);
}

export function processElement(
	this: ProcessElementContext,
	node: HtmlElement
): void {
	const slot = findSlotAttribute(node);

	/* If this element is a slot we load new metadata properties onto the element
	 * to support adding properties directly to slots, e.g. to allow/disallow
	 * content in the slot. Requires html-validate-2.7 and later */
	if (slot && haveGetMetaFor(this)) {
		const [slotname] = slot;
		loadSlotMeta(this, slotname, node);
	}

	/* Mark as having dynamic text if the element either content bound via v-html
	 * or if it is a slot. This is needed for rules with requires textual content,
	 * e.g a heading cannot be empty. */
	if (isBound(node) || isSlot(node)) {
		node.appendText(new DynamicValue(""));
	}
}
