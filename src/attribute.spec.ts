import { processAttribute as processAttributeGenerator } from "./attribute";
import { DynamicValue, AttributeData } from "html-validate";

function processAttribute(attr: AttributeData): AttributeData[] {
	return Array.from(processAttributeGenerator(attr));
}

test("should handle : as target attribute", () => {
	const attrs = processAttribute({
		key: ":foo",
		value: "bar",
	});
	expect(attrs).toEqual([
		{
			key: ":foo",
			value: "bar",
		},
		{
			key: "foo",
			value: expect.any(DynamicValue),
			originalAttribute: ":foo",
		},
	]);
});

test("should handle v-bind: as target attribute", () => {
	const attrs = processAttribute({
		key: "v-bind:foo",
		value: "bar",
	});
	expect(attrs).toEqual([
		{
			key: "v-bind:foo",
			value: "bar",
		},
		{
			key: "foo",
			value: expect.any(DynamicValue),
			originalAttribute: "v-bind:foo",
		},
	]);
});

test("should leave other attributes intact", () => {
	const attrs = processAttribute({
		key: "foo",
		value: "bar",
	});
	expect(attrs).toEqual([
		{
			key: "foo",
			value: "bar",
		},
	]);
});

test("should handle boolean attributes", () => {
	const attrs = processAttribute({
		key: "foo",
		value: undefined,
	});
	expect(attrs).toEqual([
		{
			key: "foo",
			value: undefined,
		},
	]);
});
