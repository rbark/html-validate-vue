import { Plugin } from "html-validate/build/plugin";
import configs from "./configs";
import rules from "./rules";
import transformer from "./transform";
import elementSchema from "./schema.json";
import versionCheck from "./version-check";

versionCheck();

/* eslint-disable-next-line @typescript-eslint/no-var-requires */
const pkg = require("../package.json");

const plugin: Plugin = {
	name: pkg.name,
	configs,
	rules,
	transformer,
	elementSchema,
};

export = plugin;
