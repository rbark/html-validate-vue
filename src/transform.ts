import fs from "fs";
import { Source, Transformer, TemplateExtractor } from "html-validate";
import { processAttribute } from "./attribute";
import { processElement } from "./element";

function findLocation(
	source: string,
	index: number,
	preamble: number
): [number, number] {
	let line = 1;
	let prev = 0;
	let pos = source.indexOf("\n");
	while (pos !== -1) {
		if (pos > index) {
			return [line, index - prev + preamble + 1];
		}
		line++;
		prev = pos;
		pos = source.indexOf("\n", pos + 1);
	}
	return [line, 1];
}

/* strip anything inside {{ .. }} in case it contains anything that looks like
 * markup, i.e. it will be ignored */
function stripTemplating(str: string): string {
	return str.replace(/{{(.*?)}}/g, (_, m) => {
		const filler = " ".repeat(m.length);
		return `{{${filler}}}`;
	});
}

/**
 * Returns true if the source is considered a SFC component
 */
function isVue(source: Source): boolean {
	return !!source.filename.match(/\.vue$/i);
}

/**
 * Match templates from SFC <template> tag
 */
function* transformVue(source: Source): Iterable<Source> {
	const sfc = /^(<template([^>]*)>)([^]*?)^<\/template>/gm;
	let match;
	while ((match = sfc.exec(source.data)) !== null) {
		const [, preamble, attr, data] = match;
		if (!attr.match(/lang=".*?"/)) {
			const [line, column] = findLocation(
				source.data,
				match.index,
				preamble.length
			);
			yield {
				data: stripTemplating(data),
				filename: source.filename,
				line: line + (source.line - 1),
				column: column + (source.column - 1),
				offset: match.index + (source.offset || 0) + preamble.length,
				originalData: source.originalData || source.data,
				hooks: {
					processAttribute,
					processElement,
				},
			};
		}
	}
}

/**
 * Match template property in `Vue.component` call
 */
function transformJs(source: Source): Iterable<Source> {
	const te = TemplateExtractor.fromString(source.data, source.filename);
	return Array.from(te.extractObjectProperty("template"), cur => {
		cur.originalData = source.originalData || source.data;
		cur.hooks = {
			processAttribute,
			processElement,
		};
		return cur;
	});
}

function* transform(source: Source | string): Iterable<Source> {
	/* handle html-validate@1 where the source is passed as a filename, keep
	 * making the same assumptions about the source */
	if (typeof source === "string") {
		const data = fs.readFileSync(source, "utf-8");
		yield* transform({
			data,
			filename: source,
			line: 1,
			column: 1,
			offset: 0,
		});
		return;
	}

	if (isVue(source)) {
		yield* transformVue(source);
	} else {
		yield* transformJs(source);
	}
}

transform.api = 1;

export default transform as Transformer;
