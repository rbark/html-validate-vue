import transform from "./transform";
import { processAttribute } from "./attribute";
import { Source } from "html-validate";
import { Parser } from "html-validate/build/parser";
import { Config } from "html-validate/build/config";
import { HtmlElement, DynamicValue } from "html-validate/build/dom";
import { transformFile } from "html-validate/build/transform/test-utils";

test("should extract template from single file component", () => {
	const result = transformFile(transform, "./test/component.vue");
	expect(result).toHaveLength(1);
	expect(result[0]).toMatchInlineSnapshot(`
		Object {
		  "column": 11,
		  "data": "
			<div>
				<!-- should find close-order error -->
				<p>Lorem ipsum</i>

				<!-- should handle dynamic attributes -->
				<img src=\\"image.png\\" v-bind:alt=\\"dynamicAlt\\">
				<img src=\\"image.png\\" :alt=\\"dynamicAlt\\">

				<!-- should handle @event -->
				<a @click=\\"onClick\\">click me!</a>

				<!-- should not choke on boolean attributes -->
				<input type=\\"text\\" required>

				<!-- should not choke on misc vue constructs -->
				<ul>
					<template v-for=\\"item in items\\">
						<li>{{      }}</li>
					</template>
				</ul>

				<!-- should detect duplicated :class but not duplicate class + :class -->
				<p class=\\"foo\\" :class=\\"bar\\"></p>
				<p :class=\\"foo\\" :class=\\"bar\\"></p>

				<!-- should detect bindings and friends as having textual content -->
				<h1></h1>
				<h2 v-html=\\"foo\\"></h2>
				<h3>{{     }}</h3>
				<h4><slot></slot></h4>
			</div>
		",
		  "filename": "./test/component.vue",
		  "hooks": Object {
		    "processAttribute": [Function],
		    "processElement": [Function],
		  },
		  "line": 1,
		  "offset": 10,
		  "originalData": "<template>
			<div>
				<!-- should find close-order error -->
				<p>Lorem ipsum</i>

				<!-- should handle dynamic attributes -->
				<img src=\\"image.png\\" v-bind:alt=\\"dynamicAlt\\">
				<img src=\\"image.png\\" :alt=\\"dynamicAlt\\">

				<!-- should handle @event -->
				<a @click=\\"onClick\\">click me!</a>

				<!-- should not choke on boolean attributes -->
				<input type=\\"text\\" required>

				<!-- should not choke on misc vue constructs -->
				<ul>
					<template v-for=\\"item in items\\">
						<li>{{ item }}</li>
					</template>
				</ul>

				<!-- should detect duplicated :class but not duplicate class + :class -->
				<p class=\\"foo\\" :class=\\"bar\\"></p>
				<p :class=\\"foo\\" :class=\\"bar\\"></p>

				<!-- should detect bindings and friends as having textual content -->
				<h1></h1>
				<h2 v-html=\\"foo\\"></h2>
				<h3>{{ foo }}</h3>
				<h4><slot></slot></h4>
			</div>
		</template>

		<style>
		 /* ... */
		</style>

		<script>
		/* ... */
		</script>
		",
		}
	`);
});

test("should ignore templates with preprocessors", () => {
	const result = transformFile(transform, "./test/lang.vue");
	expect(result).toHaveLength(0);
});

test("should handle missing template in single file component", () => {
	const result = transformFile(transform, "./test/missing.vue");
	expect(result).toHaveLength(0);
});

test("should extract templates from vue js component", () => {
	const result = transformFile(transform, "./test/component.js");
	expect(result).toHaveLength(2);
	expect(result[0]).toMatchInlineSnapshot(`
		Object {
		  "column": 12,
		  "data": "<p>Lorem ipsum</p>",
		  "filename": "./test/component.js",
		  "hooks": Object {
		    "processAttribute": [Function],
		    "processElement": [Function],
		  },
		  "line": 4,
		  "offset": 60,
		  "originalData": "/* globals Vue:false */

		Vue.component(\\"foo\\", {
			template: \`<p>Lorem ipsum</p>\`,
		});

		Vue.component(\\"bar\\", {
			template: \`<i>Lorem <p>ipsum</p></i>\`,
		});
		",
		}
	`);
	expect(result[1]).toMatchInlineSnapshot(`
		Object {
		  "column": 12,
		  "data": "<i>Lorem <p>ipsum</p></i>",
		  "filename": "./test/component.js",
		  "hooks": Object {
		    "processAttribute": [Function],
		    "processElement": [Function],
		  },
		  "line": 8,
		  "offset": 121,
		  "originalData": "/* globals Vue:false */

		Vue.component(\\"foo\\", {
			template: \`<p>Lorem ipsum</p>\`,
		});

		Vue.component(\\"bar\\", {
			template: \`<i>Lorem <p>ipsum</p></i>\`,
		});
		",
		}
	`);
});

test("querySelector should find class with :class present", () => {
	const parser = new Parser(Config.empty());
	const source: Source = {
		data: '<p class="foo" :class="bar"></p>',
		filename: "inline",
		line: 1,
		column: 1,
		offset: 0,
		hooks: {
			processAttribute,
		},
	};
	const doc = parser.parseHtml(source);
	const node = doc.querySelector(".foo");
	expect(node).toBeInstanceOf(HtmlElement);
	expect(Array.from(node.classList)).toEqual(["foo"]);
	expect(node.getAttribute("class", true)).toEqual([
		expect.objectContaining({
			key: "class",
			value: "foo",
		}),
		expect.objectContaining({
			key: "class",
			value: expect.any(DynamicValue),
			originalAttribute: ":class",
		}),
	]);
});

test("should be backwards compatible with html-validate@1", () => {
	const result = Array.from(transform.call(null, "./test/component.vue"));
	expect(result).toHaveLength(1);
	expect(result[0]).toMatchInlineSnapshot(`
		Object {
		  "column": 11,
		  "data": "
			<div>
				<!-- should find close-order error -->
				<p>Lorem ipsum</i>

				<!-- should handle dynamic attributes -->
				<img src=\\"image.png\\" v-bind:alt=\\"dynamicAlt\\">
				<img src=\\"image.png\\" :alt=\\"dynamicAlt\\">

				<!-- should handle @event -->
				<a @click=\\"onClick\\">click me!</a>

				<!-- should not choke on boolean attributes -->
				<input type=\\"text\\" required>

				<!-- should not choke on misc vue constructs -->
				<ul>
					<template v-for=\\"item in items\\">
						<li>{{      }}</li>
					</template>
				</ul>

				<!-- should detect duplicated :class but not duplicate class + :class -->
				<p class=\\"foo\\" :class=\\"bar\\"></p>
				<p :class=\\"foo\\" :class=\\"bar\\"></p>

				<!-- should detect bindings and friends as having textual content -->
				<h1></h1>
				<h2 v-html=\\"foo\\"></h2>
				<h3>{{     }}</h3>
				<h4><slot></slot></h4>
			</div>
		",
		  "filename": "./test/component.vue",
		  "hooks": Object {
		    "processAttribute": [Function],
		    "processElement": [Function],
		  },
		  "line": 1,
		  "offset": 10,
		  "originalData": "<template>
			<div>
				<!-- should find close-order error -->
				<p>Lorem ipsum</i>

				<!-- should handle dynamic attributes -->
				<img src=\\"image.png\\" v-bind:alt=\\"dynamicAlt\\">
				<img src=\\"image.png\\" :alt=\\"dynamicAlt\\">

				<!-- should handle @event -->
				<a @click=\\"onClick\\">click me!</a>

				<!-- should not choke on boolean attributes -->
				<input type=\\"text\\" required>

				<!-- should not choke on misc vue constructs -->
				<ul>
					<template v-for=\\"item in items\\">
						<li>{{ item }}</li>
					</template>
				</ul>

				<!-- should detect duplicated :class but not duplicate class + :class -->
				<p class=\\"foo\\" :class=\\"bar\\"></p>
				<p :class=\\"foo\\" :class=\\"bar\\"></p>

				<!-- should detect bindings and friends as having textual content -->
				<h1></h1>
				<h2 v-html=\\"foo\\"></h2>
				<h3>{{ foo }}</h3>
				<h4><slot></slot></h4>
			</div>
		</template>

		<style>
		 /* ... */
		</style>

		<script>
		/* ... */
		</script>
		",
		}
	`);
});
