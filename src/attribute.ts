import { DynamicValue, AttributeData } from "html-validate";

export function* processAttribute(
	attr: AttributeData
): Iterable<AttributeData> {
	/* always yield original attribute */
	yield attr;

	/* boolean attributes cannot be dynamic */
	if (!attr.value) {
		return;
	}

	/* setup :foo and v-bind:foo alias */
	const bind = attr.key.match(/^(?:v-bind)?:(.*)$/);
	if (bind) {
		yield Object.assign({}, attr, {
			key: bind[1],
			value: new DynamicValue(attr.value as string),
			originalAttribute: attr.key,
		});
	}
}
