import { HtmlValidate } from "html-validate";
import "html-validate/build/matchers";
import Plugin from "../src";

jest.mock("html-validate-vue", () => Plugin);

let htmlvalidate: HtmlValidate;

beforeAll(() => {
	htmlvalidate = new HtmlValidate({
		plugins: ["html-validate-vue"],
		extends: ["htmlvalidate:recommended", "html-validate-vue:recommended"],
		elements: ["html5"],
	});
});

it("should not report error for camelcase modifiers", () => {
	const report = htmlvalidate.validateString("<div v-foo.barBaz></div>");
	expect(report).toBeValid();
});
