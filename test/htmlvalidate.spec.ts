import { HtmlValidate, ConfigData } from "html-validate";
import Plugin from "../src";
import path from "path";

jest.mock("html-validate-vue", () => Plugin, { virtual: true });

const config: ConfigData = {
	plugins: ["html-validate-vue"],
	extends: ["htmlvalidate:recommended"],
	elements: [path.join(__dirname, "elements.json")],

	transform: {
		"\\.(vue|js)$": "html-validate-vue",
	},
};

test('should find errors in "component.vue"', () => {
	const htmlvalidate = new HtmlValidate(config);
	const report = htmlvalidate.validateFile("test/component.vue");
	expect(report.valid).toBeFalsy();
	expect(report.results).toMatchSnapshot();
});

test('should find errors in "component.js"', () => {
	const htmlvalidate = new HtmlValidate(config);
	const report = htmlvalidate.validateFile("test/component.js");
	expect(report.valid).toBeFalsy();
	expect(report.results).toMatchSnapshot();
});

test('should find no errors in "lang.vue"', () => {
	const htmlvalidate = new HtmlValidate(config);
	const report = htmlvalidate.validateFile("test/lang.vue");
	expect(report.valid).toBeTruthy();
	expect(report.results).toMatchSnapshot();
});

test('should find errors in "slot-metadata.vue"', () => {
	const htmlvalidate = new HtmlValidate(config);
	const report = htmlvalidate.validateFile("test/slot-metadata.vue");
	expect(report.valid).toBeFalsy();
	expect(report.results).toMatchSnapshot();
});
