import { HtmlValidate } from "html-validate";
import "html-validate/build/matchers";
import Plugin from "../src";
import glob from "glob";
import path from "path";

jest.mock("html-validate-vue", () => Plugin);

describe("smoketest", () => {
	const files = glob.sync("src/**/*.vue");
	let htmlvalidate: HtmlValidate;

	beforeAll(() => {
		htmlvalidate = new HtmlValidate({
			plugins: ["html-validate-vue"],
			extends: ["htmlvalidate:recommended", "html-validate-vue:recommended"],
			elements: [
				"html5",
				path.join(__dirname, "../elements"),
				path.join(__dirname, "elements.json"),
			],
			transform: {
				"^.*\\.vue$": "html-validate-vue",
			},
			rules: { "vue/available-slots": "error" },
		});
	});

	it.each(files)("%s", filename => {
		expect.assertions(2);
		const report = htmlvalidate.validateFile(filename);
		expect(report).toBeInvalid();
		expect(report.results[0].messages).toMatchSnapshot();
	});
});
