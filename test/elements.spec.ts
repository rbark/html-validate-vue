import { HtmlValidate, ConfigData } from "html-validate";
import "html-validate/build/matchers";
import Plugin from "../src";

jest.mock("html-validate-vue", () => Plugin, { virtual: true });

const config: ConfigData = {
	plugins: ["html-validate-vue"],
	extends: ["htmlvalidate:recommended"],
	elements: ["html5", "<rootDir>/elements.json"],

	transform: {
		"\\.(vue|js)$": "html-validate-vue",
	},
};

test("should allow <slot> when script-supporting elements is expected", () => {
	const htmlvalidate = new HtmlValidate(config);
	const report = htmlvalidate.validateString("<ul><slot></slot></ul>");
	expect(report).toBeValid();
});

describe("should handle <transition> as transparent", () => {
	test.each`
		scenario                  | markup                                                   | expected
		${"flow in flow"}         | ${"<div><transition><div></div></transition></div>"}     | ${true}
		${"flow in phrasing"}     | ${"<span><transition><div></div></transition></span>"}   | ${false}
		${"phrasing in phrasing"} | ${"<span><transition><span></span></transition></span>"} | ${true}
		${"phrasing in flow"}     | ${"<div><transition><span></span></transition></div>"}   | ${true}
	`("$scenario", ({ markup, expected }) => {
		const htmlvalidate = new HtmlValidate(config);
		const report = htmlvalidate.validateString(markup);
		expect(report.valid).toEqual(expected);
	});
});
