/* globals Vue:false */

Vue.component("foo", {
	template: `<p>Lorem ipsum</p>`,
});

Vue.component("bar", {
	template: `<i>Lorem <p>ipsum</p></i>`,
});
