# html-validate-vue changelog

## [2.3.4](https://gitlab.com/html-validate/html-validate-vue/compare/v2.3.3...v2.3.4) (2019-12-27)

## [2.3.3](https://gitlab.com/html-validate/html-validate-vue/compare/v2.3.2...v2.3.3) (2019-12-27)

## [2.3.2](https://gitlab.com/html-validate/html-validate-vue/compare/v2.3.1...v2.3.2) (2019-12-27)

## [2.3.1](https://gitlab.com/html-validate/html-validate-vue/compare/v2.3.0...v2.3.1) (2019-12-27)

### Bug Fixes

- backwards compatibility with html-validate@1 ([36b613a](https://gitlab.com/html-validate/html-validate-vue/commit/36b613ab3a37a24a6d62973c9c7c9e682216c816)), closes [html-validate/html-validate#64](https://gitlab.com/html-validate/html-validate/issues/64)

# [2.3.0](https://gitlab.com/html-validate/html-validate-vue/compare/v2.2.1...v2.3.0) (2019-12-16)

### Features

- handle slot metadata ([9104f60](https://gitlab.com/html-validate/html-validate-vue/commit/9104f6025dfcef1f73d632536676219dcb76adf6))

## [2.2.1](https://gitlab.com/html-validate/html-validate-vue/compare/v2.2.0...v2.2.1) (2019-12-08)

### Bug Fixes

- add preamble length to offset ([239e0a7](https://gitlab.com/html-validate/html-validate-vue/commit/239e0a7bc58ffc3a8712a83da554d339f01b512f))

# [2.2.0](https://gitlab.com/html-validate/html-validate-vue/compare/v2.1.0...v2.2.0) (2019-12-02)

### Features

- set `offset` property ([dfb7b32](https://gitlab.com/html-validate/html-validate-vue/commit/dfb7b32d2ab6e14d32b1e1c325357a8597b5e275))

# [2.1.0](https://gitlab.com/html-validate/html-validate-vue/compare/v2.0.1...v2.1.0) (2019-11-30)

### Features

- reconfigure `attr-case` by default ([f5da29c](https://gitlab.com/html-validate/html-validate-vue/commit/f5da29c4ce48287f08fc7004a0a571083770bad3))

## [2.0.1](https://gitlab.com/html-validate/html-validate-vue/compare/v2.0.0...v2.0.1) (2019-11-28)

### Bug Fixes

- handle slots with dashes ([b3fa646](https://gitlab.com/html-validate/html-validate-vue/commit/b3fa6467f502f3206cdc1c608f41ff2c521b0ed8))

# [2.0.0](https://gitlab.com/html-validate/html-validate-vue/compare/v1.4.0...v2.0.0) (2019-11-24)

### Features

- add `.htmlvalidate.json` for easier testing during dev ([80bea40](https://gitlab.com/html-validate/html-validate-vue/commit/80bea40d6c31b2ccbab28a8959c43398e9fd8dde))
- add recommended config ([a476f4c](https://gitlab.com/html-validate/html-validate-vue/commit/a476f4c02e25df1470199609eee74a04c5e83877))
- add required-slots rule ([9e8a100](https://gitlab.com/html-validate/html-validate-vue/commit/9e8a1007e40140fe59872ef64c3e67046bb6bb59))
- convert to plugin ([fc380e1](https://gitlab.com/html-validate/html-validate-vue/commit/fc380e1881f5117a0a0a0913a1b4edcab3d8538f))
- validate available slots ([c69a0e4](https://gitlab.com/html-validate/html-validate-vue/commit/c69a0e4e393ae5e6d277fcbd561bbb73e35b72f3))

### BREAKING CHANGES

- the plugin must be included using `plugin: ["html-validate-vue"]` and not just as a transformer.

# [1.4.0](https://gitlab.com/html-validate/html-validate-vue/compare/v1.3.2...v1.4.0) (2019-11-17)

### Features

- html-validate@2.0.0 compatibility ([17d9cbb](https://gitlab.com/html-validate/html-validate-vue/commit/17d9cbb60a46db32a97732db085da758c9da4a89))

## [1.3.2](https://gitlab.com/html-validate/html-validate-vue/compare/v1.3.1...v1.3.2) (2019-11-13)

### Bug Fixes

- add `<transition>` element ([74b8f3a](https://gitlab.com/html-validate/html-validate-vue/commit/74b8f3a8c8fcc02860a0767684d30fa1982774d1)), closes [#2](https://gitlab.com/html-validate/html-validate-vue/issues/2)

## [1.3.1](https://gitlab.com/html-validate/html-validate-vue/compare/v1.3.0...v1.3.1) (2019-10-08)

### Bug Fixes

- **transform:** handle `v-html` and `<slot>` as dynamic text ([16d5ac7](https://gitlab.com/html-validate/html-validate-vue/commit/16d5ac7)), closes [#1](https://gitlab.com/html-validate/html-validate-vue/issues/1)

# [1.3.0](https://gitlab.com/html-validate/html-validate-vue/compare/v1.2.0...v1.3.0) (2019-09-23)

### Features

- **elements:** add `elements.json` with `<slot>` declaration ([03025da](https://gitlab.com/html-validate/html-validate-vue/commit/03025da))

## 1.2.0 (2019-02-24)

- Improved attribute handling
- Rewritten in typescript

## 1.1.0 (2019-02-19)

- Handle `:foo` and `v-bind:foo` as dynamic attributes.
- Bump dependencies

## 1.0.7 (2019-02-19)

- Migrate project to gitlab.com

## 1.0.6 (2018-12-16)

- Bump html-validate to 0.16.1

## 1.0.5 (2018-11-07)

- Bump html-vlidate to 0.14.2
