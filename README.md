# html-validate-vue

Vue.js plugin for [HTML-Validate][html-validate].

- Transforms single file components and template strings.
- Augments element metadata for usage with components.
- Definitions for `<slot>` and `<transition>` elements.
- Additional rules.

[html-validate]: https://www.npmjs.com/package/html-validate

## Usage

    npm install --save-dev html-validate-vue

In `.htmlvalidate.json`:

```json
{
  "plugins": ["html-validate-vue"],
  "extends": ["htmlvalidate:recommended", "html-validate-vue:recommended"],
  "elements": ["html5"],
  "transform": {
    "^.*\\.vue$": "html-validate-vue"
  }
}
```

## Rules

| Rule                | Recommended | Description                                            |
| ------------------- | ----------- | ------------------------------------------------------ |
| vue/available-slots | Error       | Validate usage of slots. Only known slots may be used. |
| vue/required-slots  | Error       | Validate required slots. Required slots must be used.  |

## Element metadata

| Property      | Datatype   | Description              |
| ------------- | ---------- | ------------------------ |
| slots         | `string[]` | List of available slots. |
| requiredSlots | `string[]` | List of required slots.  |

### Slots

Components with slots can add element metadata for the slot using `${component}:${slot}` syntax, e.g `my-component:my-slot`.

Given a component like this:

```html
<template>
  <div>
    <div class="my-component-heading">
      <slot name="heading"></slot>
    </div>
    <div class="my-component-body">
      <slot name="body"></slot>
    </div>
    <div class="my-component-footer">
      <slot name="footer"></slot>
    </div>
  </div>
</template>
```

Metadata for the component itself is written as normally:

```json
{
  "my-component": {
    "flow": true,
    "slots": ["heading", "body", "footer"],
    "requiredSlots": ["body"]
  }
}
```

And the slots:

```json
{
  "my-component:heading": {
    "permittedDescendants": ["@heading"]
  },
  "my-component:body": {
    "permittedContent": ["@flow"]
  },
  "my-component:footer": {
    "permittedContent": ["@phrasing"]
  }
}
```

Note: unless the default slot is wrapped in `v-slot:default` it will not be validated by `my-component:default` but by the component element itself.
This can be miltigated by adding `default` as a required slot.
